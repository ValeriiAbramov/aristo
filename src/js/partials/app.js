var slider = $('.owl-carousel').owlCarousel({
    center: true,
    items: 2,
    margin: 10,
    loop: true,
    responsive: {
        600: {
            items: 3
        },
        0: {
            items: 1
        }
    }
});

$('#slide__left').on('click', function (e) {
    e.preventDefault();

    slider.trigger('prev.owl.carousel');
});

$('#slide__right').on('click', function (e) {
    e.preventDefault();

    slider.trigger('next.owl.carousel');
});